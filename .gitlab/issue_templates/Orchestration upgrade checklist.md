Upgrade manager: **`@username`**

## Preamble

- [ ] Set the title of the issue to `X.XX Configure:Orchestration upgrade` checklist
- [ ] Set the milestone of the issue

## [helm-install-image](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/)

Used by `GitLab` to install GitLab-managed apps and many ~"group::orchestration" projects downstream

- [ ] Upgrade `kubectl` minor version -
  - [ ] Review [v1.13 changelog](https://github.com/kubernetes/kubernetes/blob/release-1.13/CHANGELOG-1.13.md) for any potential breaking changes
- [ ] Upgrade `helm` binary
  - [ ] Review [helm releases](https://github.com/helm/helm/releases) for any potential breaking changes

## [auto-deploy-app](https://gitlab.com/gitlab-org/charts/auto-deploy-app)

If there are signficant unreleased changes:

- [ ] Bump the chart version in `Chart.yaml`, and the version in `template_test.go`
  - [ ] Ask a maintainer to tag the commit for that version bump with the version. This will trigger a release job for the version

## [auto-build-image](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image)

Used by the Build stage of Auto DevOps

- [ ] Update the pinned `DOCKER_VERSION` in [`.gitlab-ci.yml`](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/master/.gitlab-ci.yml). This should be done in sync with the [Auto DevOps updates](#auto-devops)

## [auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image)

Used by the Deploy stages of Auto DevOps

- [ ] Upgrade to use latest version of `helm-install-image`
- [ ] Upgrade `glibc` - Check https://github.com/sgerrand/alpine-pkg-glibc

## Auto DevOps

Each in an MR of its own:

- [ ] Update the pinned version of the docker build images `docker:X.Y.Z-dind` and `docker:X.Y.Z` (see [`docker` on Dockerhub](https://hub.docker.com/_/docker/) for the latest tags):
    * [Build template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml),
    * [Browser Performance Testing template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml)
    * [Code Quality template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml)
- [ ] Update [Deploy sub-template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml) to use latest version of `auto-deploy-image`.
- [ ] Update [DAST-Default-Branch-Deploy template](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml) to use latest version of `auto-deploy-image`

**Important:** Please run QA pipelines on each MR.

## [GitLab](https://gitlab.com/gitlab-org/gitlab)

- [ ] Update [`helm.rb`](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/kubernetes/helm.rb) to use the `HELM_VERSION` and `KUBECTL_VERSION` from the latest version of `helm-install-image`

### Ingress

- [ ] Review [nginx-ingress history](https://github.com/helm/charts/commits/master/stable/nginx-ingress) for any potential breaking changes. (NB: There is no changelog)
- [ ] Upgrade the `VERSION` in https://gitlab.com/gitlab-org/gitlab/blob/master/app/models/clusters/applications/ingress.rb

### Cert-Manager

- [ ] Read and understand the upgrade guide for the version - https://cert-manager.io/docs/installation/upgrading/
- [ ] Bump the `VERSION` in [cert_manager.rb](https://gitlab.com/gitlab-org/gitlab/blob/master/app/models/clusters/applications/cert_manager.rb).
      For some versions, the cert manager CRDs may need to be updated as well.
      If there are changes to the CRDs, only _append_ new CRDs to the post delete script.
      Do not remove CRDs from the post delete list as this will break uninstalls for older versions of cert-manager.

## [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/issues/3)

### Dockerfile

In one or more MRs (depending on the amount changes required per update):

- [ ] Update [`helm-install-image`](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image) to match `helm.rb`
- [ ] Update [`helmfile`](https://github.com/roboll/helmfile/releases). Be mindful of selecting a version compatible with the helm version
- [ ] Update [`helm-git`](https://github.com/aslafy-z/helm-git/releases)

### Ingress

- [ ] Upgrade the `version` in
      https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/blob/master/src/default-data/ingress/helmfile.yaml.
      This should match the `VERSION` in GitLab.
      Check that the [`values.yaml`](https://gitlab.com/gitlab-org/gitlab/blob/master/vendor/ingress/values.yaml) matches [GitLab's `values.yaml`](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/blob/master/src/default-data/ingress/values.yaml)

### cert-manager
- [ ] Bump the `version` in https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/blob/master/src/default-data/cert-manager/helmfile.yaml.
      This should match the `VERSION` in GitLab. Also:

    - Check that the [`values-default-issuer.yaml`](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/blob/master/src/default-data/cert-manager/values-default-issuer.yaml) matches [GitLab's `value.yaml`](https://gitlab.com/gitlab-org/gitlab/blob/master/vendor/cert_manager/values.yaml).
    - Check that the [`ClusterIssuer` template](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/blob/master/src/default-data/cert-manager-issuer/chart/templates/cluster_issuer.yaml) matches [GitLab's `ClusterIssuer` template](https://gitlab.com/gitlab-org/gitlab/blob/master/vendor/cert_manager/cluster_issuer.yaml).
    - For some versions, the cert manager CRDs may need to be updated as well.
      Update the CRDs inside https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/tree/master/src/default-data/cert-manager-crds (This should be the same CRDs referred to in `CRD_VERSION`), taking care to preserve backwards compatibility.


### Release a new version
- [ ] Ask a maintainer to tag a new version of `cluster-applications`
- [ ] Bump the image tag in https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Managed-Cluster-Applications.gitlab-ci.yml to the new version

## Administration

- [ ] Before, closing this issue, create next month's issue using template `Orchestration upgrade checklist`


/label ~"group::orchestration" ~"devops::configure" ~"dependency update"

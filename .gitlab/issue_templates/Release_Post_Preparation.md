<!-- Assure that there is an Epic to collect the release post issues for every milestone -->

## Meta

* MR: add-release-post-mr-link-here
* ~direction issue: add-main-issue-link-here

---

## Kickoff video - 16 of Month

* [ ]  Draft high points to hit in the video
* [ ]  Record and post to Youtube link:
* [ ]  Add to playlists (Ops, Configure)

---

## 1) Start the Release Post MR

* [ ]  Create the .yml file for this Release Post Item in `repo/folder`
* [ ]  Select the `Release Post Item` template for the MR and set the Milestone
* [ ]  Copy-paste in the structure for `primary.yml` or `secondary.yml`
* [ ]  Update all `@mentions` in the doc to real people. Keep PMM/Tech/Director off until you've got the content in place

## 2) Design phase

### Link to view the [current design -->](designs/Release_post_item.png)

* [ ]  Write content rough draft
* [ ]  Use Sketch mockup to make a new RPI mockup with image is needed
* [ ]  Upload mockup here to the team for feedback on the **Design Tab**
* [ ]  Export image or gif (if needed) from Sketch and upload to `/folder` of the release post
* [ ]  Post the mockup to the MR so that the reviewers can see it as well

## 3) Get team sign off that this feature works as described

See it, try it, demo it

* [ ]  PM:  @nagyv-gitlab
* [ ]  EM:  @nicholasklick
* [ ]  PD:  @?
* [ ]  Eng: @?

## 4) Head to the MR - 17 of Month

* [ ]  Start MR Checklist to verify the semantics of the file
* [ ]  Post updated designs if major edits
* [ ]  Close this issue

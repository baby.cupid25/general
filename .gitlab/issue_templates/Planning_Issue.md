<!-- Title: <group> <xy.z milestone> Planning issue -->

## Goal

Human-friendly description of where are we heading and what are we working on

1. goal 1
1. goal 2

## Tasks

Following are the proposed issues for this iteration.

<!-- For delivery status use :white_check_mark: :skull_crossbones: and :question: -->

| Issue | Category | Do we expect it to deliver? |
|-------|----------|-----------------------------|

## Actions

Close issue if all these are done:

* [ ] - once the team is aligned, set ~"workflow::ready for development",
* [ ] - create the kickoff video with the title `GitLab XX.XX Kickoff - Configure:System`
* [ ] - if there is [a kickoff issue](https://gitlab.com/gitlab-com/Product/issues?scope=all&utf8=%E2%9C%93&state=opened&search=kickoff), check it
* [ ] - create [the release post issues](https://gitlab.com/gitlab-org/configure/general/issues/new?issuable_template=Release_Post_Preparation) and add them to [an epic](https://gitlab.com/groups/gitlab-org/configure/-/epics)

/label ~"Planning Issue" ~"section::ops"
